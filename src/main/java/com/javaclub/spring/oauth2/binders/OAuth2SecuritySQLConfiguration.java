package com.javaclub.spring.oauth2.binders;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "spring.security.sql")
public class OAuth2SecuritySQLConfiguration {

    private String usersQuery;

    private String rolesQuery;
}