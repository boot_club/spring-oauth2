package com.javaclub.spring.oauth2.binders;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "jwt.certificate.key")
public class JKSKeyConfigurationBinder {

    private String alias;

    private String password;

}