package com.javaclub.spring.oauth2.binders;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "jwt.certificate.store")
public class JKSCertificateConfigurationBinder {

    private Resource file;

    private String password;
}