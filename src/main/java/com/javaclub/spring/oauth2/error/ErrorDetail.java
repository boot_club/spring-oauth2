package com.javaclub.spring.oauth2.error;

import lombok.Getter;

import java.util.Map;

/**
 * Defined error details for REST Services (reference Spring REST Book)
 * 
 * @author jduke
 *
 */
@Getter
class ErrorDetail {

	private Integer status;
	private String error;
	private String message;
	private String timeStamp;
	private String trace;
	
	ErrorDetail(int status, Map<String, Object> errorAttributes) {
		this.status = status;
		this.error = (String) errorAttributes.get("error");
		this.message = (String) errorAttributes.get("message");
		this.timeStamp = errorAttributes.get("timestamp").toString();
		this.trace = (String) errorAttributes.get("trace");
	}

}