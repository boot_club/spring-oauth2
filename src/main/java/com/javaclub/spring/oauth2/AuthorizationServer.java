package com.javaclub.spring.oauth2;

import com.javaclub.spring.oauth2.binders.JKSCertificateConfigurationBinder;
import com.javaclub.spring.oauth2.binders.JKSKeyConfigurationBinder;
import com.javaclub.spring.oauth2.util.GlobalUtils;
import com.javaclub.spring.oauth2.util.I18nService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.DefaultWebResponseExceptionTranslator;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

import javax.sql.DataSource;
import java.security.KeyPair;
import java.util.Optional;

/**
 * Generic OAuth Server using Database for Authentication Using JKS to store Private Key
 * 
 * @author Luigi
 *
 */
@Slf4j
@Configuration
@EnableAuthorizationServer
public class AuthorizationServer extends AuthorizationServerConfigurerAdapter {

	private final AuthenticationManager authenticationManager;

	private final UserDetailsService userDetailsService;

	private final DataSource dataSource;

	private final I18nService i18nService;

	private final JKSCertificateConfigurationBinder jksCertificateConfigurationBinder;

	private final JKSKeyConfigurationBinder jksKeyConfigurationBinder;

	@Autowired
	public AuthorizationServer(AuthenticationManager authenticationManager,
							   @Qualifier("customUserDetailsService") UserDetailsService userDetailsService,
							   DataSource dataSource, I18nService i18nService,
							   JKSCertificateConfigurationBinder jksCertificateConfigurationBinder,
							   JKSKeyConfigurationBinder jksKeyConfigurationBinder) {
		this.authenticationManager = authenticationManager;
		this.userDetailsService = userDetailsService;
		this.dataSource = dataSource;
		this.i18nService = i18nService;
		this.jksCertificateConfigurationBinder = jksCertificateConfigurationBinder;
		this.jksKeyConfigurationBinder = jksKeyConfigurationBinder;
	}

	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		clients.jdbc(dataSource);
	}

	@Override
	public void configure(AuthorizationServerSecurityConfigurer security) {
		security
		.tokenKeyAccess("permitAll()")
		.checkTokenAccess("isAuthenticated()");
	}

	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
		endpoints
		.tokenStore(tokenStore())
		.tokenEnhancer(jwtTokenEnhancer())
		.authenticationManager(authenticationManager)
		.userDetailsService(userDetailsService)
		.exceptionTranslator(webResponseExceptionTranslator());
	}

	@Bean
	public TokenStore tokenStore() {
		return new JwtTokenStore(jwtTokenEnhancer());
	}

	@Bean
	protected JwtAccessTokenConverter jwtTokenEnhancer() {
		KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(jksCertificateConfigurationBinder.getFile(),
				jksCertificateConfigurationBinder.getPassword().toCharArray());
		KeyPair keyPair = keyStoreKeyFactory.getKeyPair(jksKeyConfigurationBinder.getAlias(),
				jksKeyConfigurationBinder.getPassword().toCharArray());
		JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
		converter.setKeyPair(keyPair);
		return converter;
	}

	@Bean
	public WebResponseExceptionTranslator webResponseExceptionTranslator() {
		return new DefaultWebResponseExceptionTranslator() {
			@Override
			public ResponseEntity<OAuth2Exception> translate(Exception e) throws Exception {
				ResponseEntity<OAuth2Exception> responseEntity = super.translate(e);
				OAuth2Exception body = Optional.ofNullable(responseEntity.getBody())
						.orElseThrow(() ->
								new OAuth2Exception("Internal error when OAUTH2 Server tried to authenticate"));
				log.error("Exception when OAUTH2 Server tried to authenticate - Response Entity: {}, Ex Message: {}",
						responseEntity, e.getMessage());
				HttpHeaders headers = new HttpHeaders();
				headers.setAll(responseEntity.getHeaders().toSingleValueMap());
				// do something with header or response
				OAuth2Exception responseBody;
				switch (body.getOAuth2ErrorCode()) {
				case OAuth2Exception.ERROR:
					responseBody = OAuth2Exception.create(body.getOAuth2ErrorCode(),
							i18nService.getMessage(GlobalUtils.SPRING_OAUTH2_SERVER_ERROR_MESSAGE));
					break;
				case OAuth2Exception.DESCRIPTION:
					responseBody = OAuth2Exception.create(body.getOAuth2ErrorCode(),
							i18nService.getMessage(GlobalUtils.SPRING_OAUTH2_SERVER_DESCRIPTION_MESSAGE));
					break;
				case OAuth2Exception.URI:
					responseBody = OAuth2Exception.create(body.getOAuth2ErrorCode(),
							i18nService.getMessage(GlobalUtils.SPRING_OAUTH2_SERVER_URI_MESSAGE));
					break;
				case OAuth2Exception.INVALID_REQUEST:
					responseBody = OAuth2Exception.create(body.getOAuth2ErrorCode(),
							i18nService.getMessage(GlobalUtils.SPRING_OAUTH2_SERVER_INVALID_REQUEST_MESSAGE));
					break;
				case OAuth2Exception.INVALID_CLIENT:
					responseBody = OAuth2Exception.create(body.getOAuth2ErrorCode(),
							i18nService.getMessage(GlobalUtils.SPRING_OAUTH2_SERVER_INVALID_CLIENT_MESSAGE));
					break;
				case OAuth2Exception.INVALID_GRANT:
					// check invalid grant message to perform response
					switch (e.getMessage()) {
						case GlobalUtils.SPRING_OAUTH2_SERVER_LOCKED_MESSAGE:
							responseBody = OAuth2Exception.create(body.getOAuth2ErrorCode(),
									i18nService.getMessage(GlobalUtils.SPRING_OAUTH2_SERVER_ACCOUNT_LOCKED_MESSAGE));
							break;
						case GlobalUtils.SPRING_OAUTH2_SERVER_EXPIRED_MESSAGE:
							responseBody = OAuth2Exception.create(body.getOAuth2ErrorCode(),
									i18nService.getMessage(GlobalUtils.SPRING_OAUTH2_SERVER_ACCOUNT_EXPIRED_MESSAGE));
							break;
						default:
							responseBody = OAuth2Exception.create(body.getOAuth2ErrorCode(),
									i18nService.getMessage(GlobalUtils.SPRING_OAUTH2_SERVER_INVALID_GRANT_MESSAGE));
							break;
					}
					break;
				case OAuth2Exception.INSUFFICIENT_SCOPE:
					responseBody = OAuth2Exception.create(body.getOAuth2ErrorCode(),
							i18nService.getMessage(GlobalUtils.SPRING_OAUTH2_SERVER_INSUFFICIENT_SCOPE));
					break;
				default:
					responseBody = body;
					break;
				}
				return new ResponseEntity<>(responseBody, headers, responseEntity.getStatusCode());
			}
		};
	}
}
