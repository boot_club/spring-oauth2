package com.javaclub.spring.oauth2.authentication.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Collection;

@Validated
@Getter
@Setter
@Entity
@Table(name = "members")
public class Member implements Serializable {

    @Id
    @NotNull
    @Email
    @Size(min = 1, max = 128)
    @Column(name = "username")
    private String username;

    @NotNull
    @Column(name = "active")
    private Boolean active;

    @Column(name = "locked")
    private Boolean locked;

    @Column(name = "expired")
    private Boolean expired;

    @NotNull
    @Column(name = "creation_date")
    private Timestamp creationDate;


    @Column(name = "activation_date")
    private Timestamp activationDate;

    @NotNull
    @Size(min = 1, max = 64)
    @Column(name = "first_name")
    private String firstName;

    @Column(name = "middle_name")
    private String middleName;

    @NotNull
    @Size(min = 1, max = 64)
    @Column(name = "last_name")
    private String lastName;

    @Column(name = "birthday")
    private java.sql.Date birthday;

    @NotNull
    @Column(name = "password")
    private String password;

    @Column(name = "nickname")
    private String nickname;

    @Column(name = "avatar")
    private transient String avatar;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "customer_id")
    private BigInteger customerId;

    @Column(name = "last_access")
    private Timestamp lastAccess;

    @Column(name = "last_update")
    private Timestamp lastUpdate;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "members_authorities", joinColumns = @JoinColumn(name = "username"),
            inverseJoinColumns = @JoinColumn(name = "authority_id"))
    private Collection<Authority> authorities;
}
