package com.javaclub.spring.oauth2.authentication.model;

import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.Collection;

@ToString(exclude = { "password" })
public class User implements UserDetails, Serializable {

    private Collection<Authority> authorityCollection;

    private String password;

    private String username;

    private Boolean accountNonExpired;

    private Boolean accountNonLocked;

    private Boolean credentialsNonExpired;

    private Boolean enabled;

    public User(Collection<Authority> authorityCollection,
                String password,
                String username,
                Boolean accountNonExpired,
                Boolean accountNonLocked,
                Boolean credentialsNonExpired,
                Boolean enabled) {
        this.authorityCollection = authorityCollection;
        this.password = password;
        this.username = username;
        this.accountNonExpired = accountNonExpired;
        this.accountNonLocked = accountNonLocked;
        this.credentialsNonExpired = credentialsNonExpired;
        this.enabled = enabled;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorityCollection;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return ! accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return ! accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() { return ! credentialsNonExpired; }

    @Override
    public boolean isEnabled() {
        return enabled;
    }
}
