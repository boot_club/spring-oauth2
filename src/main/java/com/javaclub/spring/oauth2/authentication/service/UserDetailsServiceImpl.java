package com.javaclub.spring.oauth2.authentication.service;


import com.javaclub.spring.oauth2.authentication.model.Member;
import com.javaclub.spring.oauth2.authentication.model.User;
import com.javaclub.spring.oauth2.authentication.repository.UserRepository;
import com.javaclub.spring.oauth2.util.GlobalUtils;
import com.javaclub.spring.oauth2.util.I18nService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Custom UserDetails
 */
@Slf4j
@Service("customUserDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    private final I18nService i18nService;

    @Autowired
    public UserDetailsServiceImpl(UserRepository userRepository, I18nService i18nService) {
        this.userRepository = userRepository;
        this.i18nService = i18nService;
    }


    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return findByUsername(username);
    }

    /**
     * Build User object
     * @param username username
     * @return Object representation of User {@link User} with {@link UserDetails} data
     */
    private User findByUsername(String username) {
        Member member = findMemberByUsername(username);
        User user = new User(member.getAuthorities(),
                member.getPassword(),
                member.getUsername(),
                member.getExpired(),
                member.getLocked(),
                member.getExpired(),
                member.getActive());
        log.debug("User: {}", user);
        return user;
    }

    /**
     * Find username using repository
     * @param username username
     * @return Object representation of User {@link Member}
     */
    private Member findMemberByUsername(String username) {
        Optional.ofNullable(username)
                .orElseThrow(() -> new UsernameNotFoundException(
                        i18nService.getMessage(GlobalUtils.SPRING_OAUTH2_SERVER_INVALID_GRANT_MESSAGE)));
        Member member = userRepository.findByUsername(username);
        Optional.ofNullable(member)
                .orElseThrow(() -> new UsernameNotFoundException(
                        i18nService.getMessage(GlobalUtils.SPRING_OAUTH2_SERVER_INVALID_GRANT_MESSAGE)));
        log.debug("Member: {}", member);
        return member;
    }
}
