package com.javaclub.spring.oauth2.authentication.repository;

import com.javaclub.spring.oauth2.authentication.model.Member;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<Member, Long> {

    @Query("SELECT DISTINCT user FROM Member user " +
            "INNER JOIN FETCH user.authorities AS authorities " +
            "WHERE user.username = :username")
    Member findByUsername(@Param("username") String username);

}
