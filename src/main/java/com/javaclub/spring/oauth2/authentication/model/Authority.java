package com.javaclub.spring.oauth2.authentication.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.validation.annotation.Validated;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Validated
@Getter
@Setter
@Entity
@Table(name = "authority")
public class Authority implements GrantedAuthority, Serializable {

    @NotNull
    @Id
    @Column(name = "authority_id")
    private Integer authorityId;

    @NotNull
    @Column(name="authority_name")
    private String authorityName;

    @NotNull
    @Column(name="authority_description")
    private String authorityDescription;

    @Override
    public String getAuthority() {
        return authorityName;
    }
}
