package com.javaclub.spring.oauth2.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Slf4j
@Service
public class I18nService {

    private final MessageSource messageSource;

    @Autowired
    public I18nService(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    /**
     *
     * @param id error id
     * @return Human readable error
     */
    public String getMessage(String id) {
        Locale locale = LocaleContextHolder.getLocale();
        if (log.isDebugEnabled()) log.debug("Getting message from Locale: {}", locale.getLanguage());
        try {
            return messageSource.getMessage(id, null, locale);
        } catch (NoSuchMessageException e) {
            log.error(e.toString(), e);
            return "No Message";
        }
    }
}
