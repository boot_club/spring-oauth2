package com.javaclub.spring.oauth2.util;

public class GlobalUtils {

    public static final String SPRING_OAUTH2_SERVER_ERROR_MESSAGE = "spring.oauth2.server.error.message";

    public static final String SPRING_OAUTH2_SERVER_DESCRIPTION_MESSAGE = "spring.oauth2.server.description.message";

    public static final String SPRING_OAUTH2_SERVER_URI_MESSAGE = "spring.oauth2.server.uri.message";

    public static final String SPRING_OAUTH2_SERVER_INVALID_REQUEST_MESSAGE = "spring.oauth2.server.invalid.request.message";

    public static final String SPRING_OAUTH2_SERVER_INVALID_CLIENT_MESSAGE = "spring.oauth2.server.invalid.client.message";

    public static final String SPRING_OAUTH2_SERVER_INVALID_GRANT_MESSAGE = "spring.oauth2.server.invalid.grant.message";

    public static final String SPRING_OAUTH2_SERVER_ACCOUNT_LOCKED_MESSAGE = "spring.oauth2.server.account.locked.message";

    public static final String SPRING_OAUTH2_SERVER_ACCOUNT_EXPIRED_MESSAGE = "spring.oauth2.server.account.expired.message";

    public static final String SPRING_OAUTH2_SERVER_INSUFFICIENT_SCOPE =  "spring.oauth2.server.insufficient.scope";

    public static final String SPRING_OAUTH2_SERVER_LOCKED_MESSAGE = "User account is locked";

    public static final String SPRING_OAUTH2_SERVER_EXPIRED_MESSAGE = "User account has expired";
}
