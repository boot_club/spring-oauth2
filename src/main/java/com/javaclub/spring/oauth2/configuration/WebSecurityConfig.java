package com.javaclub.spring.oauth2.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Web Security Config for AuthenticationManager, HttpSecurity CorsConfiguration & UserDetailsService
 * @author jduke
 */

@Slf4j
@Configuration
@EnableWebSecurity
@Import(Encoders.class)
@Order(SecurityProperties.BASIC_AUTH_ORDER)
class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	private final UserDetailsService userDetailsService;

	private final BCryptPasswordEncoder passwordEncoder;

	@Autowired
	public WebSecurityConfig(@Qualifier("customUserDetailsService") UserDetailsService userDetailsService,
							 BCryptPasswordEncoder passwordEncoder) {
		this.userDetailsService = userDetailsService;
		this.passwordEncoder = passwordEncoder;
	}

	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
				.sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
				.and()
				.requestMatchers()
					.antMatchers("/swagger-ui/**")
					.and()
					.authorizeRequests()
					.antMatchers(HttpMethod.OPTIONS, "/**")
					.permitAll()
					.and()
				.requestMatchers()
					.antMatchers("/error**")
					.and()
					.authorizeRequests()
					.and()
				.requestMatchers()
					.antMatchers("/auth/**")
					.and()
					.authorizeRequests()
					.antMatchers("/health", "/info").permitAll()
					.antMatchers("/shutdown").hasAuthority("ALM")
					.anyRequest()
					.authenticated()
					.and()
					.csrf().disable()
		;
	}
    
}