# Spring Cloud AWS - Example of Authorization Server (OAuth2) #
This reference application acts as a showcase for the features provided by [Spring Cloud Security] and [Spring Security] over [Spring Boot] with [Spring-Cloud-AWS].

To check out the project and build it from source, do the following (after execute ddl & dml scripts):

    git clone git clone https://<bitbucketuser>@bitbucket.org/boot_club/spring-oauth2.git
    cd spring-oauth2
    mvn clean package -Dspring.profiles.active=test

## Running the application locally ##
**Please note that you need a running stack on AWS to run it locally!**

If you want to play around with the application, you can start it locally on your machine. 
In order to start the application you have to create a configuration file that configures the necessary
parameters to connect to the environment.

Please modify application-local.yml and set your own configuration. This file must contain all properties that you need in order to run this project.
AWS Client-ID and Client-Secret it's not necessary, however if you want to add, open your account inside the AWS console and 
retrieve them through the [Security Credentials Page] [AWS-Security-Credentials]. 

*Note:* In general we recommend that 
you use an [Amazon IAM] [Amazon-IAM] user instead of the account itself. The last password rdsPassword is used to access 
the database inside the integration tests. This password has a minimum length of 8 characters. 

An example file would look like this (using local database):

	spring:
      application:
        name: oauth2-customers
      datasource:
        url: jdbc:mariadb://localhost:3306/mydatabase?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=America/Mexico_City
        username: user
        password: pass
        hikari:
          # Hikari will use the above plus the following to setup connection pooling
          minimum-idle: 5
          maximum-pool-size: 10
          idle-timeout: 30000
          pool-name: OAuth2ServerCP
          max-lifetime: 2000000
          connection-timeout: 30000
          auto-commit: true
      jpa:
        show-sql: false
        hibernate:
          ddl-auto: none
          naming:
            physical-strategy: org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl
        properties:
          hibernate:
            #Dialect changing because: http://in.relation.to/2017/02/20/mysql-dialect-refactoring/
            dialect: org.hibernate.dialect.MySQLDialect
            format_sql: false
    
      security:
        config:
          allowedOrigin: "*"
          allowedHeader: "authorization, content-type, x-xsrf-token"
          allowedMethod: "POST, GET, OPTIONS"
    
      messages:
        basename: i18n/messages
        encoding: UTF-8
    
      thymeleaf:
        cache: false
    
    logging:
      level:
        com:
          zaxxer:
            hikari:
              HikariConfig: INFO
    
    # Our certificate settings for enabling JWT tokens
    jwt:
      certificate:
        store:
          file: classpath:/certificate/jwt.jks
          password: mySecretKey
        key:
          alias: jwt
          password: mySecretKey
    
    # Make the application available at http://localhost:8080/auth
    server:
      port: 8080
      servlet:
        context-path: /auth
    
      #ssl:
      #  key-store: classpath:/certificate/jwt.jks
      #  key-store-password: mySecretKey
      #  key-password: mySecretKey
      
    management:
      server:
        port: 9898
      #Habilita el apagado remoto
      endpoint:
        shutdown:
          enabled: true
      endpoints:
        web:
          exposure:
            include: info, health, shutdown


If you run this Application using RDS instance in AWS, you can use a default datasource configuration or AWS cloud config

	cloud:
	  aws:
	    rds:
	      myRDSName:
	        username: admin
	        password: secret
	        databaseName: mydatabasename

Where *myRDSName* is the RDS instance Application in AWS. In order to run this example, you can try only with RDS instance running

**NOTE:** Before you trying start the Application, you must be create a DB (local or AWS) in order to to that, please check the DDL and execute the SQL Scripts before run.

Once you created the YML file you can start the application using the following command:

    mvn clean package -Dspring.profiles.active=test

    java -Duser.timezone=America/Mexico_City -Dspring.cloud.config.enabled=false -Dspring.profiles.active=local -jar target/spring-oauth2-0.0.1-SNAPSHOT.jar

    
In order to get an access toke, you can try like this:

    curl -XPOST "<client_id>:<client_secret>@localhost:8080/auth/oauth/token" -d "grant_type=password&username=user@domain.com&password=verysecret"

If you want to refresh an access token, you can try like this:
    
    curl -v -X POST -d "grant_type=refresh_token&refresh_token=$REFRESH_TOKEN" --user "<client_id>:<client_secret>" localhost:8080/auth/oauth/token

*Note:* There are multiple ways to start a Spring Boot application, please refer to the 
[Spring Boot documentation] [Run-Spring-Boot] to see all the possibilities.

[Spring-Cloud-AWS]: https://github.com/spring-cloud/spring-cloud-aws
[Run-Spring-Boot]: http://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#using-boot-running-your-application
[AWS-Security-Credentials]: https://portal.aws.amazon.com/gp/aws/securityCredentials
[Amazon-IAM]: https://aws.amazon.com/iam/
[Spring Boot]: https://projects.spring.io/spring-boot/
[Spring Cloud Security]: https://cloud.spring.io/spring-cloud-security/
[Spring Security]: https://projects.spring.io/spring-security/