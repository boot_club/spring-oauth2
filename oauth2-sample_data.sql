
-- User add
INSERT INTO `members` (`username`,`active`,`locked`,`expired`,`creation_date`,`activation_date`,`first_name`,`middle_name`,`last_name`,`birthday`,`password`,`nickname`,`avatar`,`phone_number`,`last_access`,`last_update`)
VALUES ('javaclubmx@gmail.com', 1, 0, 0, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,'Luigi','Luigi','Luigi','2000-01-01','$2a$10$duDaJ719cW0GEK8M.vA82OouzL0.oKBgZJROjfWBb3Wz7VEP5eQ/K',NULL,NULL,NULL,NULL,CURRENT_TIMESTAMP);

-- AUTHORITY ADD
INSERT INTO `authority` (`authority_id`, `authority_name`, `authority_description`) VALUES ('1', 'APP_READ_DATA', 'CUSTOMER WITH READ AUTHORITY');
INSERT INTO `authority` (`authority_id`, `authority_name`, `authority_description`) VALUES ('2', 'APP_WRITE_DATA', 'CUSTOMER WITH READ & WRITE AUTHORITY);

INSERT INTO `members_authorities` (`username`, `authority_id`) VALUES ('javaclubmx@gmail.com', '1');

-- Client Details add
INSERT INTO `oauth_client_details`
(`client_id`,
`resource_ids`,
`client_secret`,
`scope`,
`authorized_grant_types`,
`web_server_redirect_uri`,
`authorities`,
`access_token_validity`,
`refresh_token_validity`,
`additional_information`,
`autoapprove`)
VALUES
('9923342189482367.app.web.mydomain.com', NULL, '$2a$10$cD3Qsa28HSg78H5ib1GpdevLdWeTM1fdGxIwVRqhVvQkG2Rxqlq3S', 'APP_CUSTOMER', 'refresh_token,password', 'http://localhost:8080/', 'PUBLIC_APP_READ,PUBLIC_APP_WRITE', '3600', '6800', NULL, '1');

