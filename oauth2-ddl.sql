CREATE DATABASE `oauth2db` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci ;

CREATE USER 'oauth2user'@'localhost' IDENTIFIED BY 'oauth2pass';
GRANT ALL PRIVILEGES ON oauth2db.* TO 'oauth2user'@'localhost' WITH GRANT OPTION;

CREATE USER 'oauth2user'@'%' IDENTIFIED BY 'oauth2pass';
GRANT ALL PRIVILEGES ON oauth2db.* TO 'oauth2user'@'%' WITH GRANT OPTION;


use oauth2db;

create table oauth_client_details (
  client_id VARCHAR(256) PRIMARY KEY,
  resource_ids VARCHAR(256),
  client_secret VARCHAR(256),
  scope VARCHAR(256),
  authorized_grant_types VARCHAR(256),
  web_server_redirect_uri VARCHAR(256),
  authorities VARCHAR(256),
  access_token_validity INTEGER,
  refresh_token_validity INTEGER,
  additional_information VARCHAR(4096),
  autoapprove VARCHAR(256)
);

create table oauth_client_token (
  token_id VARCHAR(256),
  token TEXT,
  authentication_id VARCHAR(256) PRIMARY KEY,
  user_name VARCHAR(256),
  client_id VARCHAR(256)
);

create table oauth_access_token (
  token_id VARCHAR(256),
  token TEXT,
  authentication_id VARCHAR(256) PRIMARY KEY,
  user_name VARCHAR(256),
  client_id VARCHAR(256),
  authentication TEXT,
  refresh_token VARCHAR(256)
);

create table oauth_refresh_token (
  token_id VARCHAR(256),
  token TEXT,
  authentication TEXT
);

create table oauth_code (
  code VARCHAR(256), authentication TEXT
);

create table oauth_approvals (
  userId VARCHAR(256),
  clientId VARCHAR(256),
  scope VARCHAR(256),
  status VARCHAR(10),
  expiresAt TIMESTAMP,
  lastModifiedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);


--
-- Table structure for table `members`
--
DROP TABLE IF EXISTS `members_registration_tokens`;

DROP TABLE IF EXISTS `members_roles`;

DROP TABLE IF EXISTS `members`;

CREATE TABLE IF NOT EXISTS `members` (
  `username` VARCHAR(128) NOT NULL COMMENT 'Email from username - PK value',
  `active` TINYINT NOT NULL COMMENT 'True active, false otherwise',
  `locked` TINYINT NOT NULL DEFAULT 0 COMMENT 'true if user is locked, false otherwise',
  `expired` TINYINT NOT NULL DEFAULT 0 COMMENT 'True if account expired, false otherwise',
  `creation_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Creation date',
  `activation_date` TIMESTAMP NULL COMMENT 'Activation Date',
  `first_name` VARCHAR(64) NOT NULL,
  `middle_name` VARCHAR(64) NULL,
  `last_name` VARCHAR(64) NOT NULL,
  `birthday` DATE NULL,
  `password` VARCHAR(255) NOT NULL,
  `nickname` VARCHAR(45) NULL,
  `avatar` TEXT NULL COMMENT 'Base64 image to avatar representation',
  `phone_number` VARCHAR(45) NULL COMMENT 'Phone number contact',
  `last_access` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Last user access',
  `last_update` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Last update of this row',
  PRIMARY KEY (`username`)
  )
ENGINE = InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish2_ci;

--
-- Table structure for table `members_registration_tokens`
--

CREATE TABLE IF NOT EXISTS `members_registration_tokens` (
  `token` VARCHAR(128) NOT NULL COMMENT 'Token to set password, unlock user or change password',
  `username` VARCHAR(128) NOT NULL COMMENT 'Username owner of this token',
  `creation_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expiration_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Token expiration',
  `user_action` VARCHAR(45) NULL COMMENT 'User action to use this token',
  PRIMARY KEY (`token`),
  INDEX `fk_members_registration_tokens_members_idx` (`username` ASC),
  CONSTRAINT `fk_members_registration_tokens_members`
    FOREIGN KEY (`username`)
    REFERENCES `members` (`username`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish2_ci;


--
-- Table structure for roles & members roles
--

DROP TABLE IF EXISTS `roles`;

CREATE TABLE IF NOT EXISTS `roles` (
  `role_id` INT NOT NULL COMMENT 'Role identifier',
  `role` VARCHAR(45) NOT NULL COMMENT 'Role definition',
  `role_description` VARCHAR(128) NOT NULL COMMENT 'Role Description',
  PRIMARY KEY (`role_id`))
ENGINE = InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish2_ci;

CREATE TABLE IF NOT EXISTS `members_roles` (
  `username` VARCHAR(128) NOT NULL,
  `role_id` INT NOT NULL,
  INDEX `fk_members_roles_members1_idx` (`username` ASC),
  INDEX `fk_members_roles_roles1_idx` (`role_id` ASC),
  CONSTRAINT `fk_members_roles_members1`
    FOREIGN KEY (`username`)
    REFERENCES `members` (`username`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_members_roles_roles1`
    FOREIGN KEY (`role_id`)
    REFERENCES `roles` (`role_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish2_ci;


--
-- Table structure for authority & members authorities
--
DROP TABLE IF EXISTS `members_authorities`;

DROP TABLE IF EXISTS `authority`;

CREATE TABLE IF NOT EXISTS `authority` (
  `authority_id` INT NOT NULL,
  `authority_name` VARCHAR(45) NOT NULL COMMENT 'Authority Identification',
  `authority_description` VARCHAR(128) NOT NULL COMMENT 'Large description',
  PRIMARY KEY (`authority_id`))
ENGINE = InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish2_ci;


CREATE TABLE IF NOT EXISTS `members_authorities` (
  `username` VARCHAR(128) NOT NULL,
  `authority_id` INT NOT NULL,
  INDEX `fk_members_authorities_members1_idx` (`username` ASC),
  INDEX `fk_members_authorities_authority1_idx` (`authority_id` ASC),
  CONSTRAINT `fk_members_authorities_members1`
    FOREIGN KEY (`username`)
    REFERENCES `members` (`username`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_members_authorities_authority1`
    FOREIGN KEY (`authority_id`)
    REFERENCES `authority` (`authority_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish2_ci;
